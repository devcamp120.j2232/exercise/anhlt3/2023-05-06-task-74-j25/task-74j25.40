-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2023 at 05:00 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizza365`
--

-- --------------------------------------------------------

--
-- Table structure for table `drinks`
--

CREATE TABLE `drinks` (
  `id` int(10) NOT NULL,
  `drink_code` varchar(40) NOT NULL,
  `drink_name` text NOT NULL,
  `total_money` int(10) NOT NULL,
  `create_date` int(10) NOT NULL,
  `update_date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `drinks`
--

INSERT INTO `drinks` (`id`, `drink_code`, `drink_name`, `total_money`, `create_date`, `update_date`) VALUES
(1, 'tra_tac', 'Trà tắc', 15000, 1683513223, 1683513223),
(2, 'coca', 'Coca-Cola', 20000, 1683513329, 1683513329),
(3, 'pepsi', 'Pepsi', 21000, 1683513329, 1683513329),
(4, 'tra_sua', 'Trà sữa', 30000, 1683513329, 1683513329),
(5, 'cafe', 'Coffee', 12000, 1683513329, 1683513329);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_code` varchar(40) NOT NULL,
  `pizza_size` varchar(40) NOT NULL,
  `pizza_type` varchar(40) NOT NULL,
  `voucher_id` int(10) NOT NULL,
  `total_money` int(10) NOT NULL,
  `discount` int(10) DEFAULT NULL,
  `fullname` text NOT NULL,
  `email` text DEFAULT NULL,
  `address` text DEFAULT NULL,
  `drink_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `create_date` int(10) NOT NULL,
  `update_date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_code`, `pizza_size`, `pizza_type`, `voucher_id`, `total_money`, `discount`, `fullname`, `email`, `address`, `drink_id`, `status_id`, `create_date`, `update_date`) VALUES
(1, 'O001', 'Small', 'Hải sản', 1, 240000, 10, 'Luong Tuan Anh', 'anhlt@gmail.com', '28/1 Binh Tan', 3, 1, 1683514498, 1683514498),
(2, 'O002', 'Medium', 'Hải sản', 3, 280000, 10, 'Phan Tuan Huy', 'huypt@gmail.com', 'Binh Tan', 1, 1, 1683514736, 1683514736),
(3, 'O003', 'Small', 'Thịt nướng', 2, 150000, 10, 'Nguyen Dinh Phong', 'phongnd@gmail.com', 'Q12', 2, 1, 1683514736, 1683514736),
(4, 'O004', 'Large', 'Bacon', 4, 300000, 10, 'Nguyen Trong Kha', 'khant@gmail.com', 'Q10', 3, 1, 1683514736, 1683514736),
(5, 'O005', 'Medium', 'Hải sản', 5, 250000, 10, 'Nguyen Quoc Viet', 'vietnq@gmail.com', 'Q3', 3, 1, 1683514736, 1683514736);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(10) NOT NULL,
  `status_code` varchar(40) NOT NULL,
  `status_name` text NOT NULL,
  `create_date` int(10) NOT NULL,
  `update_date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status_code`, `status_name`, `create_date`, `update_date`) VALUES
(1, '200', 'Success', 1683514128, 1683514128),
(2, '404', 'Not Found', 1683514225, 1683514225),
(3, '500', 'Internal Server Error', 1683514225, 1683514225),
(4, '401', 'Unauthorized', 1683514225, 1683514225),
(5, '403', 'Forbidden', 1683514225, 1683514225);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `user_name` varchar(40) NOT NULL,
  `first_name` varchar(40) NOT NULL,
  `last_name` varchar(40) NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `create_date` int(10) NOT NULL,
  `update_date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `first_name`, `last_name`, `email`, `password`, `create_date`, `update_date`) VALUES
(1, 'anhlt', 'Luong', 'Anh', 'anhlt@gmail.com', '123123', 1683512955, 1683512955),
(2, 'huypt', 'Phan', 'Anh', 'huypt@gmail.com', '456456', 1683513078, 1683513078),
(3, 'vietnq', 'Nguyen', 'Viet', 'vietnq@gmail.com', '789789', 1683513078, 1683513078),
(4, 'khant', 'Nguyen', 'Kha', 'khant@gmail.com', '147147', 1683513078, 1683513078),
(5, 'phongnd', 'Nguyen', 'Phong', 'phongnd@gmail.com', '258258', 1683513078, 1683513078);

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

CREATE TABLE `vouchers` (
  `id` int(10) NOT NULL,
  `voucher_code` varchar(40) NOT NULL,
  `discount` int(10) NOT NULL,
  `is_used_yn` varchar(40) NOT NULL,
  `create_date` int(10) NOT NULL,
  `update_date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vouchers`
--

INSERT INTO `vouchers` (`id`, `voucher_code`, `discount`, `is_used_yn`, `create_date`, `update_date`) VALUES
(1, '16512', 10, 'false', 1683513848, 1683513848),
(2, '95531', 15, 'false', 1683513919, 1683513919),
(3, '12354', 20, 'true', 1683513919, 1683513919),
(4, '12332', 25, 'true', 1683513919, 1683513919),
(5, '81432', 30, 'false', 1683513919, 1683513919);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `drinks`
--
ALTER TABLE `drinks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `drink_code_index` (`drink_code`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_code_index` (`order_code`),
  ADD KEY `drink_id` (`drink_id`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `voucher_id` (`voucher_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `status_code_index` (`status_code`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_name_index` (`user_name`),
  ADD UNIQUE KEY `user_email_index` (`email`) USING HASH;

--
-- Indexes for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `voucher_code_index` (`voucher_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `drinks`
--
ALTER TABLE `drinks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vouchers`
--
ALTER TABLE `vouchers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`drink_id`) REFERENCES `drinks` (`id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`),
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`voucher_id`) REFERENCES `vouchers` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
